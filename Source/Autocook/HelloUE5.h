// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HelloUE5.generated.h"

UCLASS(BlueprintType, meta=(DisplayName="你好虚幻5", ShortToolTip="第一个测试类"))
class AUTOCOOK_API AHelloUE5 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHelloUE5();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, CallInEditor)
	void SayHello();

	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(DisplayName="获取整型数据", Category="自定义函数"))
	int32 GetIntValue();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(DisplayName="我的值", Category="我的目录"))
	int32 MyValue;

	UFUNCTION() void TestUObject();
};
