#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestInterface.generated.h"

UINTERFACE(BlueprintType)
class AUTOCOOK_API UWalkable : public UInterface
{
	GENERATED_BODY()
};

class AUTOCOOK_API IWalkable
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Walk();
};

UCLASS(BlueprintType)
class AUTOCOOK_API UPerson : public UObject, public IWalkable
{
	GENERATED_BODY()
public:
	virtual void Walk_Implementation() override;
};