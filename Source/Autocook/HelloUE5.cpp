// Fill out your copyright notice in the Description page of Project Settings.


#include "HelloUE5.h"
#include "TestUObject.h"

// Sets default values
AHelloUE5::AHelloUE5()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHelloUE5::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHelloUE5::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHelloUE5::SayHello()
{
	UE_LOG(LogTemp, Log, TEXT("Hello UE5."))
}

int32 AHelloUE5::GetIntValue()
{
	return 0;
}

void AHelloUE5::TestUObject()
{
	UTestUObject* TestObject = NewObject<UTestUObject>(this);
}