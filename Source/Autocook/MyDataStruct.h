#pragma once
#include "CoreMinimal.h"
#include "MyDataStruct.generated.h"
USTRUCT(BlueprintType)
struct FMyDataStruct
{
	GENERATED_BODY()
	//想要让蓝图识别到成员变量，一定要写成BlueprintReadWrite或BlueprintReadOnly
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 IntValue;
	//默认构造函数
	FMyDataStruct() :FMyDataStruct(0) {}

	FMyDataStruct(int32 InIntValue) :IntValue(InIntValue) {}
};